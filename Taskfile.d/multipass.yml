# https://taskfile.dev

version: '3'

vars:

tasks:
  list:
    desc: "[MULTIPASS] List instances."
    vars:
      MULTIPASS_HOSTS:
        sh: multipass list
    cmds:
      - |
        printf "%-21s %-21s %s\n" "Name" "State" "IP"
        {{ range $i, $line := .MULTIPASS_CONFIG | splitLines -}}
          {{ if $line }}
            {{ $name := ($line | mustFromJson).name }}
            {{ if contains $name $.MULTIPASS_HOSTS }}
              {{ range $j, $line_host := $.MULTIPASS_HOSTS | splitLines -}}
                {{ if contains $name $line_host }}
                  {{ $state := (split " " (regexReplaceAll " +" $line_host " "))._1 }}
                  {{if eq $state "Running"}}
                    {{ $ip := (split " " (regexReplaceAll " +" $line_host " "))._2 }}
                    printf "\e[32m%-21s %-21s %s\n" {{$name}} {{$state}} {{$ip}}
                  {{ else }}
                    printf "\e[33m%-21s %-21s %s\n" {{$name}} {{$state}} "-"
                  {{ end }}
                {{ end }}
              {{ end -}}
            {{ else }}
              printf "\e[31m%-21s %s\n" {{$name}} "Missing"
            {{ end }}
          {{ end }}
        {{ end -}}
    preconditions:
      - sh: command -v multipass
        msg: "multipass is not installed"
    silent: true

  create:
    desc: "[MULTIPASS] Create instances."
    cmds:
      - |
        {{ range $i, $line := .MULTIPASS_CONFIG | splitLines -}}
          {{ if $line }} task mp:create-instance "IMAGE={{ ($line | mustFromJson).image }}" "NAME={{ ($line | mustFromJson).name }}" "CPU={{ ($line | mustFromJson).cpu }}" "MEM={{ ($line | mustFromJson).mem }}" "DISK={{ ($line | mustFromJson).disk }}" "KEY={{ ($line | mustFromJson).ansible_ssh_private_key_file }}" "MOUNT={{ ($line | fromJson).mount_point }}" {{ end }}
        {{ end -}}
    silent: true

  start:
    desc: "[MULTIPASS] Start instances."
    cmds:
      - |
        {{ range $i, $line := .MULTIPASS_CONFIG | splitLines -}}
          {{ if $line }} multipass start "{{ ($line | mustFromJson).name }}" {{ end }}
        {{ end -}}
    preconditions:
      - sh: command -v multipass
        msg: "multipass is not installed"
    silent: true

  stop:
    desc: "[MULTIPASS] Stop instances."
    cmds:
      - |
        {{ range $i, $line := .MULTIPASS_CONFIG | splitLines -}}
          {{ if $line }} multipass stop "{{ ($line | mustFromJson).name }}" {{ end }}
        {{ end -}}
    preconditions:
      - sh: command -v multipass
        msg: "multipass is not installed"
    silent: true

  del:
    desc: "[MULTIPASS] Delete instances. Arguments: [FORCE=y]"
    cmds:
      - cmd: |
             if [ "{{.FORCE}}" == "y" ]; then
               {{ range $i, $line := .MULTIPASS_CONFIG | splitLines -}}
                 {{ if $line }} ( multipass delete "{{ ($line | mustFromJson).name }}" && echo "Instance {{ ($line | mustFromJson).name }} deleted." ) || true {{ end }}
               {{ end -}}
             else
               read -p "Are you sure to delete instances? (y/[n]) " -r
               echo
               if [[ $REPLY =~ ^[Yy]$ ]]
               then
                 {{ range $i, $line := .MULTIPASS_CONFIG | splitLines -}}
                   {{ if $line }} ( multipass delete "{{ ($line | mustFromJson).name }}" && echo "Instance {{ ($line | mustFromJson).name }} deleted." ) || true {{ end }}
                 {{ end -}}
               fi
             fi
        ignore_error: true
      - multipass purge
    preconditions:
      - sh: command -v multipass
        msg: "multipass is not installed"
    interactive: true
    silent: true

  create-instance:
    vars:
      PUB_KEY: 
        sh: cat "{{.KEY}}.pub"
      FILE_REF_CLOUD_INIT_PREFIX: "{{.DIR_FILES}}/cloud-init"
      FILE_CLOUD_INIT: "{{.DIR_SSH}}/cloud-init.yml"
    cmds:
      - if test -f "{{.FILE_REF_CLOUD_INIT_PREFIX}}-{{.NAME}}.yml"; then cp -f "{{.FILE_REF_CLOUD_INIT_PREFIX}}-{{.NAME}}.yml" "{{.FILE_CLOUD_INIT}}"; else cp -f "{{.FILE_REF_CLOUD_INIT_PREFIX}}.yml" "{{.FILE_CLOUD_INIT}}"; fi
      - sed -i.bu "s#__SSH_KEY__#{{.PUB_KEY}}#g" "{{.FILE_CLOUD_INIT}}"
      - rm -f "{{.FILE_CLOUD_INIT}}.bu"
      - multipass launch "{{.IMAGE}}" --name "{{.NAME}}" --cpus "{{.CPU}}" --mem "{{.MEM}}" --disk "{{.DISK}}" --cloud-init "{{.FILE_CLOUD_INIT}}"
      - rm "{{.FILE_CLOUD_INIT}}"
      - multipass stop "{{.NAME}}"
      - sleep 5 && multipass start "{{.NAME}}"
      - |
        {{ if .MOUNT }} multipass mount "$PWD" "{{.NAME}}:{{.MOUNT}}" {{ end }}
    status:
      - if multipass list | grep -q "{{.NAME}}"; then exit 0; else exit 1; fi
    preconditions:
      - sh: command -v multipass
        msg: "multipass is not installed"
    silent: true
