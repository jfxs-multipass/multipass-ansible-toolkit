# Multipass Ansible Toolkit

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)

An easy way to deploy and provision VM with Multipass and Ansible:

* Ubuntu VMs on demand [Multipass](https://multipass.run/)
* Provisioning [Ansible](https://www.ansible.com/)

## Getting Started

### Prerequisities

In order to deploy your environment:

* Multipass
* Unix cli ssh-keygen and openssl
* [Task](https://taskfile.dev)

### Installation

#### Define the VMs

```shell
git clone https://gitlab.com/multipass_/multipass-ansible-toolkit.git
cd multipass-ansible-toolkit
```

Edit the configuration file `multipass.cfg` in `files` directory.

Add your own playbooks and/or Ansible galaxy roles in the provisioning folder.

#### Deploy and provision

Only one command.

```shell
task prov:provision
```

or step by step:

```shell
task: prov:set
task: mp:create
task: prov:inventory
task: prov:playbook
```

## Authors

* **FX Soubirou** - *Initial work* - [Gitlab repositories](https://gitlab.com/users/fxs/projects)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/multipass_/multipass-ansible-toolkit/blob/master/LICENSE) file for details.
